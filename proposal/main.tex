\documentclass[a4paper]{report}
\usepackage{float}
\restylefloat{table}
\usepackage[a4paper]{geometry}
\usepackage{titlesec}

\titleformat{\chapter}{\normalfont\huge}{\thechapter.}{20pt}{\huge\bf}
\usepackage[utf8]{inputenc}
\usepackage{hyperref}

\title{Political argumentation and prevalence of right-winged vs. left-winged speeches}
\author{Luca Kohlgraf \and Paul Häusner \and Klaus Nguetsa}
\date{\today}

\begin{document}
\maketitle

\chapter{Introduction}
The aim of this project is to examine political speeches in terms of their argumentation structure and ideology. This is based on speeches from the German Bundestag. On the basis of these speeches an argumentation analysis is made, which examines the speeches for their argumentation structure. Based on these findings, the speeches are classified according to their ideology, so that a speech can automatically be classified in a left-right scheme. Of particular interest is how the argumentation structure and vocabulary of politicians have changed over the years and at which points in time the number of left- or right-wing speeches has increased particularly strongly.

\chapter{Method}
\section{Corpus building}
\section{Argument mining}
\subsection{Argumentation Mining Process} 

To achieve our argumentation mining goal, we proceeded according to the following steps:
\begin{itemize}
\item \textit{Argumentative sentences detection :} We first have to filter our original text in order to keep only sentences that are argumentative.
\item \textit{Argumentative sentence segmentation :} Here we split the text in Argumentative discourse Units (ADU). An ADU can be a sentence or a clause resulting from the previous step. According to \cite{Rocha16}, ADUs are non-overlapping spans of text corresponding to the minimal units of arguments.
\item \textit{Relation detection and classification :} We establish the relations between ADUs and we classify those relations and we obtain an argumentation structure.
\end{itemize}

\subsection{Argumentation Mining Implementation}
For argumentative sentence detection, we used A binary Support Vector Machine Classifier. And from \cite{StedeA16} we obtained a corpus of some short argumentative texts written in German\footnote{https://github.com/peldszus/arg-microtexts}. We also needed another corpus of only non-argumentative sentence to train our classifier. But the corpus from \cite{StedeA16} contains only argumentative sentences. Therefore we construct that second corpus by hand with some random non-argumentative sentences we collected from the Web. The features we used for our classifier are presented in the following table:

\begin{table}[H]
	\centering
	\begin{tabular}{|l|l|}
		\hline
		Bigram                 & Each pair of successive words and POS tags                                                 \\ \hline
		Trigram                & Each three successive words and POS tags                                                   \\ \hline
		Adverb                 & We use a POS Tagger to detect adverbs                                                      \\ \hline
		Verb                   & Detected with a POS Tagger                                                                 \\ \hline
		Argumentative keywords & \begin{tabular}[c]{@{}l@{}}Clue words that directly indicate the presence and structure of \\ the argument contained in a span of text. \\ We construct that list of words in German by ourselves. \\ Some examples are: Deswegen, weil, außerdem...\end{tabular} \\ \hline
		Punctuation            & The sequence of punctuation marks present in the sentence is used as a feature             \\ \hline
		Parse features         & In the parse tree of each sentence we used the depth of the tree as feature                \\ \hline
	\end{tabular}
	\caption{Features used in the argument detection classifier}
\end{table}

For the Argumentative sentence segmentation, we proceeded like in \cite{Rocha16} . 
From the dataset\footnote{https://github.com/peldszus/arg-microtexts} of argumentative sentences containing already segmented argumentative sentences in XML format, we get the segments(ADUs), tokenize them and obtain a set of tokens. Each token is labeled with one of the four labels: BVP (Begin of Verb Phrase), IVP (Intermediate of Verb Phrase), EDU (Begin of
EDU) or OOO (Other).\\The
assignment of labels to each token from an argumentative sentence is performed with the following procedure:
first, the beginning of each ADU is labeled with EDU. For the remaining tokens, if the token in
position k, where k in 2,...,m and m corresponds to the length of the ADU, is a verb then it receives
the label BVP. If the token does not correspond to the beginning of an ADU and it occurs after a
token labeled with BVP, then it receives the label IVP. The remaining tokens receive the label OOO. \\

Now to perform the task(ADU boundaries detection), we train a sequential model with our labeled dataset, where the inputs are assumed to have sequential dependencies. This sequential model is based on Linear-Chain conditional random Fields (Linear-Chain CRF) which is described in \cite{LaffM2001} and \cite{Rocha16}. We used crfsuite\footnote{https://python-crfsuite.readthedocs.io/en/latest/} in python for Linear-chain Conditional random Fields to implement our model. \\
For the Features choice, we used the approach of Gil Filipe da Rocha \cite{Rocha16} . He defined features functions \(f_{j}(y_{i-1},y_{i}, x, i)\)  that take as input a sentence x, the position i of a word in the sentence, the label \(y_{i}\) of the current word, the label \(y_{i-1}\) of the previous word. The features are defined like this:
\begin{itemize}
	\item Strong punctuation mark: if \(x_{i-1}\) is strong punctuation mark then return 1; otherwise return 0. As strong punctuation marks the following tokens were considered: '.' , ';', '!' and '?' ;
	\item Weak punctuation mark : if \(x_{i-1}\) is weak punctuation mark then return 1; otherwise return 0.
	      As weak punctuation marks the following tokens were considered: ',' and ':'
	\item Part-of-speech of the word \(x_{i-1}\), \(x_{i}\) and \(x_{i+1}\) ;
	\item Lemma of the word \(x_{i-1}\), \(x_{i}\) and \(x_{i+1}\) ;
	\item Begin of sentence : if i == 0 return 1; otherwise return 0
	\item End of sentence : if i == sentencelength return 1; otherwise return 0;
\end{itemize}

Finally, for the last step of our process which consists of predicting the relations between the ADUs(argumentation structure) we still used a Liner-Chain CRF Model. What differs from the previous model is the input, which in this case is a couple of ADUs representing a premise and a conclusion. Again accordingly we constructed a training set from the dataset mentioned above since it has an argument structure represented in XML in each short text. The features remained the same like the previous step.\\
In \cite{Moens18} they used a Context-free Grammar to achieve this task and they reported 60 \% accuracy in detecting the argumentation structures. Our own results are resumed in the next section. 

\section{Prevalence of left- and right-winged speeches}
The analysis of the occurrence of left and right speeches in the German Bundestag is, for several reasons, a difficult matter. On the one hand, the classification of a political actor in a left-right schema is a subjective criterion which is always based on the opinion of the classifier. Thus different persons can come to a different assessment. Therefore a left-right-scheme is also always a model to be questioned, especially because it reduces complicated political contents to a very simple model. This raises the question of whether such a model can adequately reflect reality. In order to be able to make an exact classification, a political classification of each member of the Bundestag is actually necessary, since each political actor has a different political conviction. However, this seems almost impossible with the current number of 709 MPs in the German Bundestag. A simplification is therefore necessary in order to be able to assign the members of the Bundestag more easily to a political direction. Here it makes sense to use the factions, which mostly consist of the individual party members of a party represented in the Bundestag. Thus only one classification of the parliamentary group and/or party must be made. \\ \\

In the current legislative period (19th), six different parliamentary groups were able to form \footnote{https://www.bundestag.de/parlament/fraktionen}. Two MEPs resigned from an existing parliamentary group and are now non-attached in the Bundestag. The so-called Union faction, consisting of the Christian Democratic Union of Germany (CDU) and the Christian Social Union in Bavaria (CSU), provides most of the deputies. Both parties represent Christian Democratic values and are assigned to the centre-right spectrum by political scientists. The second largest faction, which together with the CDU/CSU franction forms the current government, is the faction of the Social Democratic Party of Germany (SPD). Also represented in the Bundestag by a parliamentary group are the Free Democratic Party (FDP) and Bündnis 90/Die Grünen (Greens). What all these parties have in common is that they are predominantly enacted in the political centre \cite{DeckerN18}. Together they make up 546 of the 709 deputies, so that only a small part of the deputies in the current Bundestag can be clearly assigned to a left or right position. The group whose members clearly and unambiguously stand for a left-wing policy is that of the Left Party. A clearly right-wing party did not exist in the Bundestag for many years. Since the last Bundestag election in 2018, the Alternative for Germany (AfD) of a right-wing party has succeeded in entering parliament. This shows that right-wing positions were previously not permanently represented in the Bundestag, but could only be represented by a few individual deputies. \\ \\

The positions of left- or right-leaning speeches can often be identified from the topics addressed. For example, a left-wing speech addresses more the question of social justice, while a right-wing speech deals with the security of the country \cite{Bobbio96}. In the Bundestag, on the other hand, all parties give speeches on a certain topic, so that it is not possible to tell from the chosen topics whether the speech is left- or right-leaning. Thus, the position of the speaker can often only be determined by linguistic nuances. Thus, it is often only through linguistic nuances that it is possible to determine the position of the speaker, such as the diversity of the vocabulary used, and the use of words that members of other parties do not use. This analysis is based on the speeches of the 19th legislative period. The conspicuous features of speeches by the left or the AfD are noted. Then the speeches of the legislative periods 13 to 18, in which no right-wing party was represented in the Bundestag, are examined. This compares whether other parties can also be perceived as conspicuous and thus possibly adopt a clear political position. \\ \\
An automated classification of speeches into the right-left-scheme with an SVM or a Bayesian classifier, realized with scikit-learn, did not make sense after several attempts, since speeches in the Bundestag are often topic-dependent and the classifier, after training with speeches of one legislative period, could not correctly assign speeches from another legislative period to a political direction.
\chapter{Results \& Evaluation}
\section{Argument Mining}
\paragraph{}
For argumentative sentences detection we got the following results:
\begin{table}[H]
	\centering
	\begin{tabular}{|l|l|l|l|}
		\hline
		      & Precision & Recall & F-score \\ \hline
		Arg   & 0.82      & 0.84   & 0.83    \\ \hline
		NoArg & 0.45      & 0.42   & 0.44    \\ \hline
	\end{tabular}
	\caption{Results from the detection of argumentative sentences}
\end{table}
We obtained better overall results in the detection of argumentative sentences as
compared to the results obtained in the detection of non argumentative sentences, which we associate with the very small number of non argumentative sentences contained in our dataset.
\paragraph{}
For argumentative sentences segmentation(ADU detection) we got the following results:

\begin{table}[H]
	\centering
	\begin{tabular}{|l|l|l|l|}
		\hline
		            & precision & recall & f1-score \\ \hline

		EDU         & 0.966     & 0.869  & 0.915    \\ \hline
		OOO         & 0.934     & 0.696  & 0.798    \\ \hline
		BVP         & 0.985     & 0.993  & 0.989    \\ \hline
		IVP         & 0.878     & 0.977  & 0.925    \\ \hline
		avg / total & 0.914     & 0.910  & 0.907    \\ \hline
	\end{tabular}
	\caption{Results from the ADUs detection}
\end{table}
Here our scores were really good in general. That means we could predict each label with a high precision. The label BVP had the best score(0.989) due to the feature that gives the part of speech. That feature actually indicates directly the label BVP. The label OOO had the lowest f1-score, which
we understand because this label has the lowest number of examples.
\paragraph{}
For relations detection( argumentative structure) we got the following results
\begin{table}[H]
	\centering
	\begin{tabular}{|l|l|l|l|}
		\hline
		  & Precision & Recall & F-score \\ \hline
		  & 0.531     & 0.495  & 0.462   \\ \hline
	\end{tabular}
	\caption{Results from the relations detection}
\end{table}

Here our precision was 0.531 due to the fact that we didn't have enough relations between arguments in our training set. Another reason is that our model is restricted to the sentence level without considering the general context of an argument, the other arguments in the Neighborhood and we did not introduce any semantic feature in order to catch the sense of a relation between ADUs, which could be a really powerful feature on this task.

\section{Prevalence of left- and right-winged speeches}


\chapter{Conclusion}

\bibliographystyle{alpha}
\bibliography{references}

\end{document}
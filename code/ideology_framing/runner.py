from corpus.schema import find_speeches_by_leg
from vocabulary_analysis import get_vocabulary_of_party, get_lexical_diversity_of_party

speeches = []


def load_speeches(from_leg=13, to_leg=19):
    global speeches
    speeches = []
    for i in range(from_leg, to_leg + 1):
        speeches += find_speeches_by_leg(i)


def run_ideology_framing_analysis(leg_num):
    with open('analysis' + leg_num + '.txt', 'w') as result_file:
        result_file.write('Vocabulary analysis\n')

        result_file.write(45 * '- ' + '\n\n')

        load_speeches(leg_num, leg_num)

        result_file.write('Vocabulary size\n\n')
        voc_linke = get_vocabulary_of_party(['DIE LINKE.', 'PDS'], speeches)
        result_file.write('The Left: {}\n'.format(len(voc_linke)))
        voc_spd = get_vocabulary_of_party(['SPD'], speeches)
        result_file.write('SPD: {}\n'.format(len(voc_spd)))
        voc_gruene = get_vocabulary_of_party(['DIE GRÜNEN/BÜNDNIS 90', 'GRÜNE', 'BÜNDNIS 90/DIE GRÜNEN'], speeches)
        result_file.write('The Greens: {}\n'.format(len(voc_gruene)))
        voc_fdp = get_vocabulary_of_party(['FDP'], speeches)
        result_file.write('FDP: {}\n'.format(len(voc_fdp)))
        voc_cducsu = get_vocabulary_of_party(['CDU', 'CSU'], speeches)
        result_file.write('CDU, CSU: {}\n'.format(len(voc_cducsu)))
        voc_afd = get_vocabulary_of_party(['AfD'], speeches)
        result_file.write('AfD: {}\n\n'.format(len(voc_afd)))

        result_file.write('Lexical Diversity\n\n')
        dif_linke = get_lexical_diversity_of_party(['DIE LINKE.', 'PDS'], speeches)
        result_file.write('The Left: {}\n'.format(dif_linke))
        dif_spd = get_lexical_diversity_of_party(['SPD'], speeches)
        result_file.write('SPD: {}\n'.format(dif_spd))
        dif_gruene = get_lexical_diversity_of_party(['DIE GRÜNEN/BÜNDNIS 90', 'GRÜNE', 'BÜNDNIS 90/DIE GRÜNEN'],
                                                    speeches)
        result_file.write('The Greens: {}\n'.format(dif_gruene))
        dif_fdp = get_lexical_diversity_of_party(['FDP'], speeches)
        result_file.write('FDP: {}\n'.format(dif_fdp))
        dif_cducsu = get_lexical_diversity_of_party(['CDU', 'CSU'], speeches)
        result_file.write('CDU, CSU: {}\n'.format(dif_cducsu))
        dif_afd = get_lexical_diversity_of_party(['AfD'], speeches)
        result_file.write('AfD: {}\n'.format(dif_afd))

        voc_without_linke = sorted(set(voc_spd + voc_gruene + voc_afd + voc_cducsu + voc_fdp))
        voc_only_linke = [word for word in voc_linke if word not in voc_without_linke]
        result_file.write('Vocab of the Left: \n')
        result_file.write('{}'.format(voc_only_linke))
        voc_without_afd = sorted(set(voc_spd + voc_gruene + voc_linke + voc_cducsu + voc_fdp))
        voc_only_afd = [word for word in voc_afd if word not in voc_without_afd]
        result_file.write('Vocab of the Left: \n')
        result_file.write('{}'.format(voc_only_afd))


run_ideology_framing_analysis(19)

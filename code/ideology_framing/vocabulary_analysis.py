from corpus.schema import find_by_speaker_id


def filter_speeches_by_parties(parties, speeches):
    result = []
    for speech in speeches:
        speaker = find_by_speaker_id(speech.speaker_id)
        if speaker[0].party in parties:
            result.append(speech)
    return result


def get_vocabulary_of_party(party, speeches):
    vocabulary = build_vocabulary(party, speeches)
    return sorted(set(vocabulary))


def get_lexical_diversity_of_party(party, speeches):
    vocabulary = build_vocabulary(party, speeches)
    return len(get_vocabulary_of_party(party, speeches)) / len(vocabulary)


def build_vocabulary(party, speeches):
    speeches_party = filter_speeches_by_parties(party, speeches)
    speeches_text = [speech.text.split() for speech in speeches_party]
    vocabulary = []
    for speech in speeches_text:
        vocabulary += speech
    return vocabulary

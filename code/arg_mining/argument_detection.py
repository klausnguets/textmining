import codecs
import os
import pickle
import xml.etree.ElementTree as ET

import nltk
import spacy
from fuzzywuzzy import fuzz
from nltk import Tree

woerter = codecs.open("woerter.txt", "r", "utf-8")
list_woerter = woerter.readlines()
woerter.close()
list_woerter = [wort[:-1].lower() for wort in list_woerter if len(wort) > 1]


def get_corpus():
    corpus = ''
    tree = ET.parse('2018/19042-data.xml')
    root = tree.getroot()
    for rede in root.iter('rede'):
        for p in rede.iter('p'):
            corpus = corpus + str(p.text) + '\n'
    sentences = nltk.sent_tokenize(corpus, language='german')
    return sentences


def get_matching_ratio(expression, sentence):
    boundary = len(sentence) - len(expression)
    ratios = []
    max_ratio = -1
    pos = 0
    for i in range(0, boundary):
        ratio = fuzz.ratio(expression, sentence[i:len(expression) + i])
        ratios.append(ratio)
        if ratio > max_ratio:
            max_ratio = ratio
            pos = i

    return max_ratio


def get_matching_ratio_word(expression, sentence):
    sentence_words = nltk.tokenize.word_tokenize(sentence, language='german')
    ratios = []
    max_ratio = -1
    pos = 0
    max_pos = 0
    for w in sentence_words:
        ratio = fuzz.ratio(expression, w)
        ratios.append(ratio)
        if ratio > max_ratio:
            max_ratio = ratio
            max_pos = pos
        pos = pos + 1
    return max_ratio


def argumentative_word_feature(sentence):
    for wort in list_woerter:
        word_count = len(nltk.tokenize.word_tokenize(wort, language='german'))
        if word_count > 1:
            ratio = get_matching_ratio(wort, sentence)
        else:
            ratio = get_matching_ratio_word(wort, sentence)
        if ratio > 90:
            return True
    return False


# def each_word_feature(sentence):
#   sentence_words = nltk.tokenize.word_tokenize(sentence, language='german')
#  return  sentence_words


def adverb_feature(sentence):
    tags = tag_feature(sentence)
    count = 0
    for i in tags:
        if i == "ADV":
            count = count + 1
    return str(count)


def bigrams(sentence):
    tokens = nltk.tokenize.word_tokenize(sentence, language='german')
    bigrm = list(nltk.bigrams(tokens))
    return bigrm


def trigrams(sentence):
    tokens = nltk.tokenize.word_tokenize(sentence, language='german')
    trigrm = list(nltk.trigrams(tokens))
    return trigrm


def pos_bigrams(sentence):
    tokens = tag_feature(sentence)
    bigrm = list(nltk.bigrams(tokens))
    return bigrm


def pos_trigrams(sentence):
    tokens = tag_feature(sentence)
    trigrm = list(nltk.trigrams(tokens))
    return trigrm


def tag_feature(sentence):
    list_tags = []
    nlp = spacy.load('de')
    doc = nlp(sentence)
    for t in doc:
        list_tags.append(t.pos_)
    return list_tags


def punctuation_feature(sentence):
    punctuations = []
    sentence_words = nltk.tokenize.word_tokenize(sentence, language='german')
    for word in sentence_words:
        if word in ";.-,?!":
            punctuations.append(word)
    return punctuations


def to_nltk_tree(node):
    if node.n_lefts + node.n_rights > 0:
        return Tree(node.orth_, [to_nltk_tree(child) for child in node.children])
    else:
        return node.orth_


def parse_tree_feature(sentence):
    for char in sentence:
        if char in ",?.!\/;:":
            sentence = sentence.replace(char, '')
    nlp = spacy.load('de')
    doc = nlp(sentence)
    if len(list(doc.sents)) == 1 and len(nltk.tokenize.word_tokenize(sentence, language='german')) >= 2:
        for sent in doc.sents:
            t = to_nltk_tree(sent.root)
            return {"height": t.height(), "leaves": len(t.leaves())}
    else:
        return {"height": 0, "leaves": 0}


def general_feature(sentence):
    i = 1
    tree = parse_tree_feature(sentence)
    features = {"argumentative_word": argumentative_word_feature(sentence), "parseTreeHeight": tree["height"],
                "parseTreeLeaves": tree["leaves"]}
    # pbig=POS_bigrams(sentence)
    # for it in pbig:
    #    features["POS_bigram"+str(i)]='-'.join(it)
    #    i=i+1
    # i=1
    # ptrig=POS_trigrams(sentence)
    # for it in ptrig:
    #    features["POS_trigram"+str(i)]='-'.join(it)
    #    i=i+1
    # i=1
    tags = tag_feature(sentence)
    if "AUX" in tags:
        features["hasModalAuxiliary"] = True
    for it in tags:
        features["tag" + str(i)] = it
        i = i + 1
    i = 1
    punctuations = punctuation_feature(sentence)
    for it in punctuations:
        features["punctuation" + str(i)] = it
        i = i + 1
    features["sentence_length"] = len(sentence)
    sentence_words = nltk.tokenize.word_tokenize(sentence, language='german')
    features["number_words"] = len(sentence_words)
    features["adverbs_number"] = adverb_feature(sentence)

    pbig = pos_bigrams(sentence)
    for it in pbig:
        features["has_posbigram_" + '-'.join(it)] = True

    ptrig = pos_trigrams(sentence)
    for it in ptrig:
        features["has_postrigram_" + '-'.join(it)] = True

    return features


# print(tag_feature('Tom in unsere Fußballmannschaft aufzunehmen war ganz klar ein Fehler'))
# print(punctuation_feature('- Tom in unsere Fußballmannschaft, aufzunehmen; war ganz klar ein Fehler.'))
# print(general_feature('- Tom in unsere Fußballmannschaft, aufzunehmen; war ganz klar ein Fehler.'))

path = "arg-microtexts-master/corpus/de/"
directory = os.fsencode(path)
corpus = ''
for file in os.listdir(directory):
    filename = os.fsdecode(file)
    if filename.endswith(".txt"):
        filerReader = codecs.open(path + filename, "r", "utf-8")
        content = filerReader.readlines()
        corpus = corpus + content[0]
        filerReader.close()
sentences = nltk.sent_tokenize(corpus, language='german')
corpus2 = ''
filerReader = codecs.open("noargs_draft.txt", "r", "utf-8")
content2 = filerReader.readlines()
content2 = [line[:-1] for line in content2]
for line in content2:
    corpus2 = corpus2 + line
filerReader.close()

# sentences2 = nltk.sent_tokenize(corpus2,language='german')
# classifier = nltk.classify.SklearnClassifier(LinearSVC())
# featuresets = [(general_feature(s), "arg") for s in sentences]
# for s in sentences2:
#    featuresets.append((general_feature(s),"noarg"))
# classifier.train(featuresets)
#
# save_classifier = open("clf_arg_sentences.pickle","wb")
# pickle.dump(classifier, save_classifier)
# save_classifier.close()

classifier_f = open("clf_arg_sentences.pickle", "rb")
classifier = pickle.load(classifier_f)
classifier_f.close()
bundestag = get_corpus()
for st in bundestag:
    print(st + "->" + classifier.classify(general_feature(st)))

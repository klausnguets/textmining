import pickle

import nltk
import spacy


def tag_feature(sentence):
    list_tags = []
    nlp = spacy.load('de')
    doc = nlp(sentence)
    for t in doc:
        list_tags.append(t.pos_)
    return list_tags


def is_strong_punctuation(word):
    if word in ".;!?":
        return str(1)
    return str(0)


def is_weak_punctuation(word):
    if word in ",:":
        return str(1)
    return str(0)


def word2features(sent, postags, i):
    word = sent[i]
    postag = postags[i]
    features = [
        'bias',
        'word.lower=' + word.lower(),
        'word[-3:]=' + word[-3:],
        'word[-2:]=' + word[-2:],
        'word.isupper=%s' % word.isupper(),
        'word.istitle=%s' % word.istitle(),
        'word.isdigit=%s' % word.isdigit(),
        'postag=' + postag,
        'postag[:2]=' + postag[:2],

    ]
    if i > 0:
        word1 = sent[i - 1]
        postag1 = postags[i - 1]
        features.extend([
            '-1:word.lower=' + word1.lower(),
            '-1:word.istitle=%s' % word1.istitle(),
            '-1:word.isupper=%s' % word1.isupper(),
            '-1:postag=' + postag1,
            '-1:postag[:2]=' + postag1[:2],
            '-1:strongpunctuation' + is_strong_punctuation(word1),
            '-1:weakpunctuation' + is_weak_punctuation(word1),
        ])
    else:
        features.append('BOS')

    if i < len(sent) - 1:
        word1 = sent[i + 1]
        postag1 = postags[i + 1]
        features.extend([
            '+1:word.lower=' + word1.lower(),
            '+1:word.istitle=%s' % word1.istitle(),
            '+1:word.isupper=%s' % word1.isupper(),
            '+1:postag=' + postag1,
            '+1:postag[:2]=' + postag1[:2],
        ])
    else:
        features.append('EOS')

    return features


def sent2features(sent, postags):
    return [word2features(sent, postags, i) for i in range(len(sent))]


def get_sentences_and_labels():
    list_sentences = []
    save_edus = open("edu_relations.pickle", "rb")
    content = pickle.load(save_edus)
    save_edus.close()

    for pre, con in content:
        line = pre + con
        val = {}
        # line = line.split(' ')
        # line[len(line)-2]=line[len(line)-2]+line[len(line)-1]
        # line=line[:-1]
        # line=[word[4:] for word in line]
        line = [word for word in line if word[5:] != "''"]
        tokens = [word[5:] for word in line]
        labels = [word[:4] for word in line]
        val["labels"] = labels
        val["words"] = tokens
        tags = tag_feature(' '.join(tokens))
        for i in range(1, 5): tags.append("MARG" + str(i))
        val["POS"] = tags
        # print(pre)
        # print(con)
        list_sentences.append(val)
    return list_sentences


# train_sentences=getSentencesAndLabels()
# #
# X_train = [sent2features(s["words"], s["POS"]) for s in train_sentences]
# y_train = [s["labels"] for s in train_sentences]
#
#
# crf = sklearn_crfsuite.CRF(
#     algorithm='lbfgs',
#     c1=0.1,
#     c2=0.1,
#     max_iterations=100,
#     all_possible_transitions=True
# )
# crf.fit(X_train, y_train)
#
# save_classifier = open("clf_relations.pickle","wb")
# pickle.dump(crf, save_classifier)
# save_classifier.close()

classifier_f = open("clf_relations.pickle", "rb")
crf = pickle.load(classifier_f)
classifier_f.close()

st = "Deshalb ist es uns wichtig, dass wir umweltfreundliche Alternativen beim Bordstrom organisieren und es möglich " \
     "machen, eine mobile Landstromversorgung zu garantieren. Ich kann nur sagen: Das sind die richtigen Schritte, " \
     "die wir gemeinsam als Koalition gehen wollen. "
tokens = nltk.tokenize.word_tokenize(st, language='german')
postags = tag_feature(st)
test = [sent2features(tokens, postags)]
print(len(tokens))
print(len(postags))
print(tokens)
y = crf.predict(test)

print(y)

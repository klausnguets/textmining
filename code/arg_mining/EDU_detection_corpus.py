import os
import pickle
import xml.etree.ElementTree as ET

import nltk
import spacy


def get_edus():
    path = "arg-microtexts-master/corpus/de/"
    directory = os.fsencode(path)
    edu_corpus = []
    for file in os.listdir(directory):
        filename = os.fsdecode(file)
        if filename.endswith(".xml"):
            tree = ET.parse(path + filename)
            root = tree.getroot()
            for edu in root.iter('edu'):
                edu_value = edu.text

                edu_corpus.append(str(edu.text))
    return edu_corpus


def get_word_pos(word):
    nlp = spacy.load('de')
    doc = nlp(word)
    return doc[0].pos_


def tag_feature(sentence):
    list_tags = []
    nlp = spacy.load('de')
    doc = nlp(sentence)
    for t in doc:
        list_tags.append(t.pos_)
    return list_tags


def get_labeled_edu_sentences(edus):
    edu_corpus = ''
    returned_edus = []
    for edu in edus:
        tags = tag_feature(edu)
        for i in range(1, 5):
            tags.append("MARG" + str(i))

        tokens = nltk.tokenize.word_tokenize(edu, language='german')
        tokens[0] = "EDU/" + tokens[0]
        k = 0
        verb_found = False
        for word in tokens:
            if k > 0 and (tags[k] == "VERB" or tags[k] == "AUX"):
                tokens[k] = "BVP/" + tokens[k]
                verb_found = True
            elif tags[k] != "VERB" and verb_found:
                tokens[k] = "IVP/" + tokens[k]
            else:
                if k > 0:
                    tokens[k] = "OOO/" + tokens[k]
            k = k + 1

        # new_edu=' '.join(tokens[:len(tokens)-1])+tokens[len(tokens)-1]
        new_edu = ' '.join(tokens)
        # print(new_edu)
        edu_corpus = edu_corpus + new_edu + ' '

    edu_sentences = nltk.sent_tokenize(edu_corpus, language='german')
    return edu_sentences


eduSentences = get_labeled_edu_sentences(get_edus())

# save_corpus = open("labeled_corpus.txt", "w")
# for s in eduSentences:
#    save_corpus.write(s + '\n')

save_edus = open("edu_tags.pickle", "wb")
pickle.dump(eduSentences, save_edus)
save_edus.close()

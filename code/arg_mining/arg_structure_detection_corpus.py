import os
import pickle
import xml.etree.ElementTree as ET

import nltk
import spacy


def tag_feature(sentence):
    list_tags = []
    nlp = spacy.load('de')
    doc = nlp(sentence)
    for t in doc:
        list_tags.append(t.pos_)
    return list_tags


def get_relations():
    path = "arg-microtexts-master/corpus/de/"
    directory = os.fsencode(path)
    relations_corpus = []
    edus = {}
    for file in os.listdir(directory):
        filename = os.fsdecode(file)
        if filename.endswith(".xml"):
            tree = ET.parse(path + filename)
            root = tree.getroot()

            for edu in root.iter('edu'):
                edu_value = edu.text
                id = edu.get("id")
                edus[id] = edu_value

            # aduId=id.replace('e', 'a')

            for adu in root.iter('adu'):
                if adu.get('type') == 'pro' or adu.get('type') == 'opp':
                    relation = {"text": edus[adu.get("id").replace('a', 'e')], "attack": [], "support": [], "rest": []}
                    for edge in root.iter('edge'):
                        if (edge.get('type') == 'sup' or edge.get('type') == 'exa') and edge.get('src') == adu.get(
                                'id'):
                            relation["support"].append(edus[(edge.get('trg')).replace('a', 'e')])

                        if edge.get('type') == 'reb' and edge.get('src') == adu.get('id'):
                            relation["attack"].append(edus[(edge.get('trg')).replace('a', 'e')])

                    for key, value in edus.items():
                        if value not in relation["attack"] and value not in relation["support"] and not key == adu.get(
                                "id").replace('a', 'e'):
                            relation["rest"].append(value)

                    relations_corpus.append(relation)

    return relations_corpus


def relations_tagging(type, edu):
    if type == "attack":
        tedu = "AEDU/"
        bvp = "ABVP/"
        ivp = "AIVP/"
        ooo = "AOOO/"
    elif type == "support":
        tedu = "SEDU/"
        bvp = "SBVP/"
        ivp = "SIVP/"
        ooo = "SOOO/"
    elif type == "nothing":
        tedu = "NEDU/"
        bvp = "NBVP/"
        ivp = "NIVP/"
        ooo = "NOOO/"

    tags = tag_feature(edu)
    for i in range(1, 5): tags.append("MARG" + str(i))

    tokens = nltk.tokenize.word_tokenize(edu, language='german')
    tokens[0] = tedu + tokens[0]
    k = 0
    verb_found = False
    for word in tokens:
        if k > 0 and (tags[k] == "VERB" or tags[k] == "AUX"):
            tokens[k] = bvp + tokens[k]
            verb_found = True
        elif tags[k] != "VERB" and verb_found:
            tokens[k] = ivp + tokens[k]
        else:
            if k > 0:
                tokens[k] = ooo + tokens[k]
        k = k + 1
    return tokens


def get_combination_tagging():
    relations = get_relations()
    list_relations = []
    # normal relations construction
    for r in relations:
        premisse = r["text"]
        for a in r["attack"]:
            list_relations.append((relations_tagging("attack", premisse), relations_tagging("attack", a)))
            list_relations.append((relations_tagging("nothing", a), relations_tagging("nothing", premisse)))

            # for a2 in r["attack"]:
            #   if a2!=a:
            #      list_relations.append((relations_tagging("nothing", a2), relations_tagging("nothing", a)))

        for s in r["support"]:
            list_relations.append((relations_tagging("support", premisse), relations_tagging("support", s)))
            list_relations.append((relations_tagging("nothing", s), relations_tagging("nothing", premisse)))

            # for s2 in r["support"]:
            #   if s2!=s:
            #      list_relations.append((relations_tagging("nothing", s2), relations_tagging("nothing", s)))

        # for n in r["rest"]:
        # list_relations.append((relations_tagging("nothing", premisse), relations_tagging("nothing", n)))
        # list_relations.append((relations_tagging("nothing", n), relations_tagging("nothing", premisse)))

        # for n2 in r["rest"]:
        # if n2!=n:
        # list_relations.append((relations_tagging("nothing", n2), relations_tagging("nothing", n)))

        # print(list_relations)
    return list_relations


relations = get_combination_tagging()
save_edus = open("edu_relations.pickle", "wb")
pickle.dump(relations, save_edus)
save_edus.close()

reader = open("edu_relations.pickle", "rb")
relat = pickle.load(reader)
reader.close()
# for r in relat: print(r)
# print(len(getRelations()))
# print(relations_tagging("attack", "Außerdem darf der Müll auch nicht straffrei liegengelassen werden."))

# for r in getRelations(): print(r)

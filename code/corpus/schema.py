from sqlalchemy import Column, Integer, String, Date, ForeignKey, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy.sql import functions

from .util import get_db_url

engine = create_engine(get_db_url())
Session = sessionmaker(bind=engine)

Base = declarative_base()


#
# Schemas
#
class AbstractSchema:
    id = Column(Integer, primary_key=True)

    def save(self):
        session = Session()
        session.add(self)
        session.commit()

    def __repr__(self):
        return '<id {}>'.format(self.id)


class Speech(Base, AbstractSchema):
    __tablename__ = 'speech'

    # topic_id = Column(Integer, ForeignKey('topic.id))
    text = Column(String)
    legislature_period = Column(Integer)
    legislature_session = Column(Integer)
    date = Column(Date)

    speaker_id = Column(Integer, ForeignKey('mdb.id'))
    mdb = relationship('Mdb', back_populates='speeches')


class Mdb(Base, AbstractSchema):
    __tablename__ = 'mdb'

    title = Column(String(250))
    first_name = Column(String(250))
    last_name = Column(String(250))
    place = Column(String(250))
    party = Column(String(250))

    speeches = relationship('Speech', back_populates='mdb')


#
# Schema Methods
#
def find_speeches_by_leg(leg_number):
    session = Session()
    result = []
    try:
        for u in session.query(Speech.text, Speech.speaker_id).filter(Speech.legislature_period == leg_number):
            result.append(u)
    finally:
        session.close()
    return result


def find_by_speaker_id(speaker_id):
    session = Session()
    result = []
    try:
        for u in session.query(Mdb.id, Mdb.first_name, Mdb.last_name, Mdb.party).filter(Mdb.id == speaker_id):
            result.append(u)
    finally:
        session.close()
    return result


def find_by_one_name(name):
    session = Session()
    result = []
    try:
        for u in session.query(Mdb.id, Mdb.first_name, Mdb.last_name, Mdb.party). \
                filter(functions.concat(Mdb.first_name, ' ', Mdb.last_name).like('%' + name + '%')):
            result.append(u)
    finally:
        session.close()
    return result


def find_mdb_by_last_name(lastname):
    session = Session()
    result = []
    try:
        for u in session.query(Mdb.id, Mdb.first_name, Mdb.party). \
                filter(Mdb.last_name.like(lastname)):
            result.append(u)
    finally:
        session.close()

    return result


def find_mdb_by_full_name(firstname, lastname):
    session = Session()
    result = []
    try:
        for u in session.query(Mdb.id, Mdb.party). \
                filter(Mdb.first_name.like('%' + firstname + '%')). \
                filter(Mdb.last_name.like('%' + lastname + '%')):
            result.append(u)
    finally:
        session.close()

    return result


def find_mdb_by_full_name_and_party(firstname, lastname, party):
    session = Session()
    result = []
    try:
        for u in session.query(Mdb.id, Mdb.party). \
                filter(Mdb.first_name == firstname). \
                filter(Mdb.last_name == lastname). \
                filter(Mdb.party == party):
            result.append(u)
    finally:
        session.close()

    return result


#
# Schema options
#
def destroy_tables(mdb=True, speech=True):
    if engine.dialect.has_table(engine, 'speech'):
        Speech.__table__.drop(engine)
    if engine.dialect.has_table(engine, 'mdb'):
        # TODO: drop cascade?
        Mdb.__table__.drop(engine)


def create_tables():
    if not engine.dialect.has_table(engine, 'mdb'):
        Mdb.__table__.create(engine)

    if not engine.dialect.has_table(engine, 'speech'):
        Speech.__table__.create(engine)

import os
import re
from io import BytesIO
from zipfile import ZipFile

import requests

from .schema import Speech, find_mdb_by_full_name, find_by_one_name, destroy_tables, create_tables, \
    find_mdb_by_full_name_and_party
from .util import open_all_xmls, german_date

# supported legislature periods
CURRENT_MIN_SUPPORTED = 13
CURRENT_MAX_SUPPORTED = 19

SPEECH_URLS = {18: 'https://www.bundestag.de/blob/490392/bd1411276e8a1a64ac41ad3781193b64/pp18-data.zip',
               17: 'https://www.bundestag.de/blob/490378/866ff042fe66d9ee46a9c955b2993dca/pp17-data.zip',
               16: 'https://www.bundestag.de/blob/490386/680a2cc89eb71317b256bb44509ca17e/pp16-data.zip',
               15: 'https://www.bundestag.de/blob/490394/6e9a38a40bb79abdd7fe6d74f56708ec/pp15-data.zip',
               14: 'https://www.bundestag.de/blob/490380/1d83b7e383f9f09d88bd9e89aba07fb0/pp14-data.zip',
               13: 'https://www.bundestag.de/blob/490388/84914a1feff6f2f4988ce352a5500845/pp13-data.zip'
               }
DATA_ROOT = '../data'

# name unused words:
LAST_NAME_REG = re.compile('(von )|(der )|(de )')
FIRST_NAME_REG = re.compile(
    '(Dr\. )|(Alterspräsident )|( Graf)|( Freiherr von)|(Präsident(in)? )|(Vizepräsident(in)? )')
ROLE_REG = re.compile(' (Parl\. Staatssekretär(in)?( |\w)*)|Wehrbeauftragter des Deutschen Bundestages|Bundeskanzlerin|'
                      'Bundeskanzler|(Bundesminister(in)?( |\w)*)|Staatsminister(in)?( |\w)*')
NAME_REG = re.compile(' (van|von der|von|de)')
TITLE_REG = re.compile('h\. c\. ')


def fetch_speech_data(leg_num):
    if not os.path.exists(os.path.join(DATA_ROOT, "leg" + str(leg_num))):
        url = requests.get(SPEECH_URLS[leg_num])
        zipfile = ZipFile(BytesIO(url.content))
        zipfile.extractall(os.path.join(DATA_ROOT, "leg" + str(leg_num)))


def read_data_leg(leg_number):
    """
    Read the data from one legislature period which uses to be 4 years in Germany.
    :param leg_number: The number of the period beginning with the first German Bundestag
    """
    # source data for legislature
    data_path_leg = os.path.join(DATA_ROOT, "leg" + str(leg_number))

    # current legislature period
    legislature = str(leg_number)

    # handle data from legislature 19; data is not in TEI format
    if legislature == '19':
        session = 0
        i = 0
        for soup in open_all_xmls(data_path_leg, True):
            date = german_date(soup.vorspann.kopfdaten.datum.attrs["date"])
            session += 1

            all_speeches = [s for s in soup.find_all("rede")]
            for speech_instance in all_speeches:
                name = speech_instance.find("name")
                first_name = re.sub(FIRST_NAME_REG, '', name.vorname.text)
                first_name = re.sub('in der', 'In der', first_name)
                last_name = re.sub(LAST_NAME_REG, '', name.nachname.text)
                user = find_mdb_by_full_name(first_name, last_name)

                if user.__len__() == 0:
                    # if no user found try full name search
                    full_name = first_name + ' ' + last_name

                    # special cases for special persons
                    # if full_name == 'Eberhardt Alexander Gauland':
                    #     full_name = 'Alexander Gauland'
                    # elif full_name == 'Michael Georg Link':
                    #     full_name = 'Michael Link'

                    user = find_by_one_name(full_name)

                    if user.__len__() == 0:
                        continue

                speaker_ref = user[0]

                # get speech text
                all_text = [t.text for t in speech_instance.find_all("p") if t["klasse"] in ["Z", "J", "O"]]
                speech = " ".join(all_text)

                if speech.strip().__len__() == 0:
                    continue

                db_speech = Speech(text=speech, legislature_period=leg_number, legislature_session=session, date=date,
                                   speaker_id=speaker_ref.id)
                i += 1
                db_speech.save()
        print('[speech] Added {} speeches to the database'.format(i))
        return i
    else:
        i = 0
        for soup in open_all_xmls(data_path_leg, False):
            date = german_date(soup.find('DATUM').text, convert='%d.%m.%Y')
            session = soup.find("NR").text
            session = session[session.find('/') + 1:]
            text = soup.find('TEXT').text
            text = text[text.find("Beginn:"):]
            all_speeches = split_text(text)
            for speaker_id, speech in all_speeches.items():
                text = " ".join(speech).replace('\n', ' ')
                speech = Speech(text=text, legislature_period=leg_number, legislature_session=session, date=date,
                                speaker_id=speaker_id)
                print('Speech {} in {}/{} by {}:{}'.format(i, leg_number, session, speaker_id, text))
                speech.save()
                i += 1
        print('[speech] Added {} speeches to the database'.format(i))
        return i


def split_text(text):
    speeches = {}
    text = text.split('\n\n')
    actual_speaker = None
    speech = []
    for p in text:
        if p.startswith('(') and p.endswith(')'):
            continue
        speaker = p.find("):")
        if speaker == -1:
            speech.append(p)
            continue
        party = p[p.rfind('(') + 1:speaker].replace('-', '').replace('\n', '')
        if '\n' in p[:p.rfind('(')]:
            name_str = p[p.find('\n') + 1:p.rfind('(') - 1].strip()
        else:
            name_str = p[:p.rfind('(')].strip()
        if '(' in name_str:
            name_str = name_str[:name_str.find('(')].strip()
        name_str = re.sub(FIRST_NAME_REG, '', name_str)
        name_str = re.sub(NAME_REG, '', name_str)
        name_str = re.sub(TITLE_REG, '', name_str)
        user = find_by_one_name(name_str)
        if not user:
            firstname = name_str[:name_str.rfind(' ')].strip()
            lastname = name_str[name_str.rfind(' '):].strip()
            user = find_mdb_by_full_name_and_party(firstname, lastname, party)
        if user:
            if actual_speaker:
                speeches[actual_speaker[0].id] = speech
            speech = p[p.rfind('\n'):].split()
            actual_speaker = user
    return speeches


def clear_speech_db():
    destroy_tables(mdb=False, speech=True)


def parse_speeches(min_leg=CURRENT_MIN_SUPPORTED, max_leg=CURRENT_MAX_SUPPORTED):
    # delete previously written data
    # clear_speech_db()
    create_tables()
    all_speeches = 0
    for i in range(min_leg, max_leg + 1):
        # read data leg and write to db
        fetch_speech_data(i)
        all_speeches += read_data_leg(i)
    print(all_speeches)

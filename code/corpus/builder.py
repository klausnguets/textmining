import sys

from .mdb_parser import parse_mdb_data, clear_mdb_table
from .speech_parser import parse_speeches, clear_speech_db
from .util import query_yes_no


class BuilderExit(BaseException):
    pass


class Builder:

    def __init__(self):
        self.clear = False
        self.mdb = False
        self.speeches = False

    def parse_command_line(self):
        args = sys.argv
        if len(args) > 4:
            return False

        i = 0

        while i < len(args):
            if args[i] == '-h' or args[i] == '--help':
                self.show_usage(sys.stdout)
                return True
            elif args[i] == '-c' or args[i] == '--clear':
                self.clear = True
            elif args[i] == '-m' or args[i] == '--mdb':
                self.mdb = True
            elif args[i] == '-s' or args[i] == '--speech':
                self.speeches = True
            i += 1

        if not (self.clear or self.mdb or self.speeches):
            return False
        else:
            return True

    @staticmethod
    def show_usage(out):
        out.write(
            "Usage: {} --<options> \n"
            "clear      Clear the database\n"
            "mdb        Insert members of parliament in database\n"
            "speech     Insert speeches in database\n"
            "help       Show this synopsis\n".format(sys.argv[0]))

    def run(self):
        if not self.parse_command_line():
            self.show_usage(sys.stderr)
            raise BuilderExit()

        if self.clear:
            if query_yes_no('[clear] All data from the database will be removed do you want to proceed?', default=None):
                clear_mdb_table()
                clear_speech_db()
                print('[clear] All data was removed')
            else:
                print('[clear] No data removed or added')
                sys.exit(0)

        if self.mdb:
            parse_mdb_data()

        if self.speeches:
            parse_speeches()


def main():
    try:
        builder = Builder()
        builder.run()
    except BuilderExit:
        sys.exit()
    except KeyboardInterrupt:
        sys.exit()

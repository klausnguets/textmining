import datetime
import os
import re
import sys

from bs4 import BeautifulSoup

DATA_PATH = '../data'


def get_db_url():
    user = 'textminingproject'
    password = '12345678'
    host = 'localhost'
    port = '15432'
    db = 'postgres'

    url = 'postgresql://{}:{}@{}:{}/{}'
    return url.format(user, password, host, port, db)


def query_yes_no(question, default="yes"):
    """Ask a yes/no question via input() and return their answer.

    :param question is a string that is presented to the user.
    :param default is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).
    :return The "answer" return value is True for "yes" or False for "no".
    """
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            print("Please respond with 'yes' or 'no' "
                  "(or 'y' or 'n').")


def german_date(string, convert=None):
    """
    Converts a string to a german date format date. The string should be in format 'dd.mm.yyyy' if the string is in
    another date format it can be changed by setting the convert parameter to the actual convert of the string.
    :param string The string that should be converted
    :param convert The date format of the input string
    """
    if string is None:
        return None

    if convert is not None:
        date = datetime.datetime.strptime(string, convert).strftime('%d.%m.%Y')
    else:
        date = string

    return datetime.datetime.strptime(date, "%d.%m.%Y").date()


def open_all_xmls(directory, delete_dtd=False):
    """
    Generator for all files with xml ending from a given directory opened with BeautifulSoup. The encoding should be
    utf-8.
    :param directory: The directory where the data is
    :param delete_dtd: If some meta info from the xml should be deleted to avoid invalid xml syntax e.g.
    the xml stylesheet must be deleted.
    :return: The opened xml files
    """
    only_data = [f for f in os.listdir(directory) if re.match("(.*)\.xml", f)]

    # take every file from directory
    for file_name in only_data:
        # open with utf-8 encoding
        with open(directory + "/" + file_name, 'r', encoding="utf8") as file:
            opened_file = file.read()

            # delete xml-stylesheet and if option enabled
            if delete_dtd:
                re.sub("<\?xml-stylesheet(.)*>", "", opened_file)

            # yield back the opened xml file
            yield BeautifulSoup(opened_file, features="lxml")

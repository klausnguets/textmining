from __future__ import print_function

import os
import re
from io import BytesIO
from zipfile import ZipFile

import requests
from bs4 import BeautifulSoup

from .schema import destroy_tables, create_tables, Mdb

# Setup links and local folders for data
MDB_LINK = 'https://www.bundestag.de/blob/472878/c1a07a64c9ea8c687df6634f2d9d805b/mdb-stammdaten-data.zip'
MDB_FILE_NAME = '../data/mdbs/MDB_STAMMDATEN.XML'


def fetch_mdb_data():
    if not os.path.exists(MDB_FILE_NAME):
        url = requests.get(MDB_LINK)
        zipfile = ZipFile(BytesIO(url.content))
        zipfile.extractall("../data/mdbs")


def clear_mdb_table():
    destroy_tables(mdb=True, speech=False)


def parse_mdb_data():
    print('[mdb] Start fetching required data')
    fetch_mdb_data()
    print('[mdb] Start reading fetched data')

    with open(MDB_FILE_NAME, 'r', encoding='utf-8') as file:
        soup = BeautifulSoup(file.read(), 'xml')
        members = soup.findAll('MDB')

        print('[mdb] Start inserting data into database')
        destroy_tables(mdb=True, speech=False)
        create_tables()

        for member in members:
            member_names = member.findAll('NAME')
            for name in iter(member_names):
                # collect data for db
                member_title = name.find('ANREDE_TITEL').text
                member_firstname = name.find('VORNAME').text
                member_lastname = name.find('NACHNAME').text
                member_place = name.find('ORTSZUSATZ').text
                member_place = re.sub('\(', '', member_place)
                member_place = re.sub('\)', '', member_place)
                member_party = member.find('PARTEI_KURZ').text
                Mdb(title=member_title, first_name=member_firstname, last_name=member_lastname, place=member_place,
                    party=member_party).save()

        # # Add ministers
        # Mdb("", "Svenja", "Schulze", "", "SPD").save()
        # Mdb("", "Franziska", "Giffey", "", "SPD").save()
        # Mdb("", "Verena", "Bentele", "", "SPD").save()
        # Mdb("", "Andreas", "Geisel", "", "SPD").save()
        #
        # # Add parliament members with new name
        # Mdb("", "Katharina", "Willkomm", "", "FDP").save()

        print('[mdb] ' + str(len(members) + 1) + ' members of parliament inserted into database')
